#eliminar la base de datos
DROP DATABASE if EXISTS articulos;
/*
crear y seleccionar la base de datos
*/

CREATE DATABASE articulos;

USE articulos; -- seleccionar bd

CREATE TABLE articulos(
id  INT AUTO_INCREMENT,
nombre VARCHAR(200),
categoria INT,
precio FLOAT,
stock BOOL, 
fecha DATE,
PRIMARY KEY (id)

);




CREATE TABLE categorias (

id INT, 
nombre VARCHAR(200),
descripcion varchar(500),
numero INT ,
PRIMARY KEY (id)

);
