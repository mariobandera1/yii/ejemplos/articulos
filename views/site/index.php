<?php

/** @var yii\web\View $this */

$this->title = 'Gestion de producto';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Gestion de artículos y categorias</h1>

        <p class="lead">Aplicacion para realizar la gestión de mis productos y de las categorias</p>

       
    </div>

    <?= yii\helpers\Html::img("@web/imgs/articulos.jpg") ?>
</div>
